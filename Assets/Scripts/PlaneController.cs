﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class PlaneController : MonoBehaviour
{
    public Texture2D MainTexture;
    public Texture2D BrashTexture;
    public Texture2D ErasterPointerSprite;
    public Texture2D ErasterSprite;
    private Compute compute = new Compute();
    public SpriteRenderer Pointer;
    public Camera MCamera;
    private Texture2D SelectedBrash;
    public List<Texture2D> Brashes = new List<Texture2D>();
    private bool continuousDrawing = false;
    private Vector2 point;
    private Vector2 oldPoint;
    private int brashSize = 64;
    private Renderer HolstRenderer;

    public Color brushColor = Color.red;
    private bool takenBrash = false;
    private int selectedBrush = 0;

    private float coefSizeBrash = 0;

    public Text SizeBrashText;
    public Slider SizeBrashSlider;
    public List<Button> BrashButtons = new List<Button>();
    public Button EraserButton;
    public Image ColorImage;
    public Slider RSlider;
    public Slider GSlider;
    public Slider BSlider;

    void Start ()
    {
        StartUpFacilities();
    }

    void StartUpFacilities()
    {
        HolstRenderer = transform.GetComponent<Renderer>();
        coefSizeBrash = 0.06f / 64;
        compute.SetMainTexture(MainTexture);

        SizeBrashSlider.onValueChanged.AddListener(delegate { SizeSlider(); });
        SizeBrashSlider.minValue = 10;
        SizeBrashSlider.maxValue = 512;
        SizeBrashSlider.value = 64;

        ColorImage.color = brushColor;

        TakeBrash(0);
        EraserButton.onClick.AddListener(delegate { TakeEraser(); });

        RSlider.value = brushColor.r;
        GSlider.value = brushColor.g;
        BSlider.value = brushColor.b;
        RSlider.onValueChanged.AddListener(delegate { SetColor(); });
        GSlider.onValueChanged.AddListener(delegate { SetColor(); });
        BSlider.onValueChanged.AddListener(delegate { SetColor(); });
    }
	
	void Update ()
    {
        if (Input.GetMouseButton(0))
        {
            HitForDraw(true);
        }
        else
        {
            continuousDrawing = false;
            HitForDraw(false);
        }
    }

    void HitForDraw(bool draw)
    {
        Ray ray = MCamera.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit))
        {
            Pointer.gameObject.SetActive(true);
            Cursor.visible = false;
            Pointer.transform.position = new Vector3(hit.point.x, hit.point.y, Pointer.transform.position.z);

            if (draw)
            {
                Draw(hit);
            }
        }
        else
        {
            Pointer.gameObject.SetActive(false);
            Cursor.visible = true;
        }
    }

    void Draw(RaycastHit hit)
    {
        point = InPixels(hit.textureCoord2);
        DrawOnPosition(point);

        /*для того чтоб соединять две отдаленные точки между собой при отдалении между апдейтами, 
        что -бы не было разрыва линии (я не смог придумать оптимизацию этого, мотому решил закомментировать данную функцию, однако уверен, 
        что решение есть, но пока я его не отыскал).
        if (continuousDrawing)
        {
            //FillingGap(point, oldPoint);
        }

        oldPoint = point;
        continuousDrawing = true;*/
    }

    void FillingGap(Vector2 position, Vector2 oldPosition)
    {
        float dist = Vector2.Distance(oldPosition, position);
        float countPoints = dist / (brashSize / 4);
        if (countPoints >= 2)
        {
            for (int i = 1; i < countPoints; i++)
            {
                DrawOnPosition(oldPosition + ((position - oldPosition) * i / countPoints));
            }
        }
        else if (countPoints > 1)
        {
            DrawOnPosition(oldPosition + ((position - oldPosition) * dist / 2));
        }
    }

    void DrawOnPosition(Vector2 positions)
    {
        HolstRenderer.material.mainTexture = compute.BitmapsAddMix((Texture2D)HolstRenderer.material.mainTexture, brushColor, positions.x, positions.y) as Texture;
    }

    Vector2 InPixels(Vector2 pos)
    {
        pos.x *= MainTexture.width;
        pos.y *= MainTexture.height;
        return pos;
    }

    void SizeSlider()
    {
        brashSize = (int)SizeBrashSlider.value;
        compute.SetBrash(BrashTexture, brashSize);
        SizeBrashText.text = "Размер кисти: " + SizeBrashSlider.value.ToString();
        Pointer.size = new Vector2(coefSizeBrash * brashSize, coefSizeBrash * brashSize);
    }

    public void TakeBrash(int num)
    {
        takenBrash = true;
        BrashButtons[selectedBrush].interactable = true;
        selectedBrush = num;
        BrashButtons[selectedBrush].interactable = false;
        EraserButton.interactable = true;
        BrashTexture = Brashes[selectedBrush];
        compute.SetBrash(BrashTexture, brashSize);
        brushColor = ColorImage.color;
        Pointer.color = brushColor;
        Pointer.sprite = Sprite.Create(Brashes[selectedBrush], new Rect(0.0f, 0.0f, Brashes[selectedBrush].width, Brashes[selectedBrush].height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    void TakeEraser()
    {
        takenBrash = false;
        BrashButtons[selectedBrush].interactable = true;
        EraserButton.interactable = false;
        BrashTexture = ErasterSprite;
        compute.SetBrash(BrashTexture, brashSize);
        brushColor = Color.white;
        Pointer.color = Color.white;
        Pointer.sprite = Sprite.Create(ErasterPointerSprite, new Rect(0.0f, 0.0f, ErasterPointerSprite.width, ErasterPointerSprite.height), new Vector2(0.5f, 0.5f), 100.0f);
    }

    void SetColor()
    {
        Color col = new Color(RSlider.value, GSlider.value, BSlider.value);
        ColorImage.color = col;
        brushColor = takenBrash ? col : brushColor;
        Pointer.color = takenBrash ? col : brushColor;
    }
}
