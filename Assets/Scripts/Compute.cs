﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Compute
{
    private Color[] mainColor;
    private Color[] spriteColor;
    private Color[] resultColor;
    private int pointX = 0;
    private int pointY = 0;
    private int mainColInd;
    private int spriteColInd;
    private Texture2D spriteTexture;

    public void SetMainTexture(Texture2D mainTexture)
    {
        mainColor = mainTexture.GetPixels();
        resultColor = mainTexture.GetPixels();
    }

    public void SetBrash(Texture2D texture, int size)
    {
        spriteTexture = Resize(texture, size, size);
        spriteColor = spriteTexture.GetPixels();
    }

    public Texture2D BitmapsAddMix(Texture2D mainTexture, Color color, float x, float y)
    {
        int firstPointX = (int)x - spriteTexture.width / 2;
        int firstPointY = (int)y - spriteTexture.height / 2;

        int i = 0;
        int j = firstPointY < 0 ? Mathf.Abs(firstPointY) : 0;
        while (j < spriteTexture.height)
        {
            i = firstPointX < 0 ? Mathf.Abs(firstPointX) : 0;
            pointY = j + firstPointY;
            while (i < spriteTexture.width)
            {
                pointX = i + firstPointX;
                mainColInd = pointY * mainTexture.width + pointX;
                if (mainColInd >= resultColor.Length || pointX >= mainTexture.width)
                {
                    break;
                }
                spriteColInd = j * spriteTexture.width + i;
                resultColor[mainColInd] = spriteColor[spriteColInd].a != 0 ? color : resultColor[mainColInd];
                i++;
            }
            j++;
        }
        Texture2D resultTexture = new Texture2D(mainTexture.width, mainTexture.height);
        resultTexture.SetPixels(resultColor);
        resultTexture.Apply();
        return resultTexture;
    }

    public static Texture2D Resize(Texture2D source, int newWidth, int newHeight)
    {
        source.filterMode = FilterMode.Point;
        RenderTexture rt = RenderTexture.GetTemporary(newWidth, newHeight);
        rt.filterMode = FilterMode.Point;
        RenderTexture.active = rt;
        Graphics.Blit(source, rt);
        Texture2D nTex = new Texture2D(newWidth, newHeight);
        nTex.ReadPixels(new Rect(0, 0, newWidth, newWidth), 0, 0);
        nTex.Apply();
        RenderTexture.active = null;
        return nTex;
    }
}
